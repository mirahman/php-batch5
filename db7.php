<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$host = "localhost";
$user = "root";
$password = "";
$database = "batchfive";

$dsn = "mysql:host=$host;dbname=$database";

try {

$dbh = new PDO($dsn,  $user, $password);


$sql = "select * from students";

$stmt = $dbh->prepare($sql);

$stmt->execute();

$strCSV = "";

while($row = $stmt->fetch(PDO::FETCH_ASSOC))
{
    $strCSV .= implode(",", $row)."\n";
}
    
$fileName = "./export.csv";
$fp = fopen($fileName, "w");
fwrite($fp, $strCSV);
fclose($fp);

downloadFile($fileName);

} catch (Exception $e) {
    echo $e->getMessage();
}


function downloadFile($file = "") {
    if (file_exists($file)) {
        ob_clean();
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment;filename=' . basename($file));
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file));
        readfile($file);
        exit;
    }
}