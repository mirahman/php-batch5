<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

final class Cricketer {
    
    public $name;
    public $position;
    public $age;
    
    public function __construct($name, $position, $age) {
        $this->name = $name;
        $this->position = $position;
        $this->age = $age;
    }
    
    public final function showName() {
        echo $this->name;
    }
    
    public function getAge() {
        return self::$age;
    }
    
}

class NationalCricketer extends Cricketer {
    
    public $division;
    
    public function __construct($name, $position, $age, $division) {
        parent::__construct($name, $position, $age);
        $this->division = $division;
    }
    
    public function showName() {
        echo "I am not going to show name";
    }
    
}

class ClubCricketer extends NationalCricketer {
    
    public $clubName;
    
     public function __construct($name, $position, $age, $division, $clubName) {
         parent::__construct($name, $position, $age,$division);
        $this->clubName = $clubName;
    }
}

$mosaddek = new Cricketer("Mosaddek Saikat", "All Rounder", 20);
$mash = new Cricketer( "Masrafee", "Bowler", 32);

$mash->showName()."<br />";

$nafis = new NationalCricketer("Shariar Nafis", "Opening Batsman", 30, "Barisal");

echo $nafis->showName();

