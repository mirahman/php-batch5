<?php

interface DB {
    
    public function doQuery(string $sql);
    
    public function fetchResult();
    
}

class mymysql implements DB {
    public function doQuery(string $sql) {
	pg_query();
    }
    
    public function fetchResult() {
	
    }
}

class Connect {
    
    function __construct() {

    $obj = new mymysql();
    }
}

$db = new Connect();

$db->doQuery();