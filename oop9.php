<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Stats {
    
    public $run;
    public $wicket;
    public $catch;
    
    public function __construct($run, $wicket, $catch) {
        $this->run = $run;
        $this->wicket = $wicket;
        $this->catch = $catch;
    }
    
}

class Cricketer {
    
    public $name;
    public $position;
    public $age;
    public $stat;
    
    public function __construct($name, $position, $age, $stat) {
        $this->name = $name;
        $this->position = $position;
        $this->age = $age;
        $this->stat = $stat;
    }
    
    public function showName() {
        echo $this->name." : ".$this->stat->run."<br />";
    }
    
    public function __clone() {
        echo "I am cloning now <br />";
        //print_r($this->stat);
        
        $newStat = new Stats($this->stat->run, $this->stat->wicket, $this->stat->catch);
        $this->stat = $newStat;
        
    }
    
}


$stats1 = new Stats("45", "3", "1");

$mosaddek = new Cricketer("Mosaddek Saikat", "All Rounder", 20, $stats1);

$sakib = clone $mosaddek;

$sakib->name = "Sakib al Hasan";
$sakib->stat->run = 5000;

$mosaddek->showName()."<br>";
$sakib->showName()."<br>";
