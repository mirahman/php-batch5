<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

trait Security {
    
    public $userName;
    public $userType;
    
    public function showUserName() {
	echo "I am showing User Name";
    }
    
    public function showUserType() {
	echo "I am showing User type";
    }
    
}


trait Export {
    
    public $type;
    public $content;
    
    public function pdf() {
	echo "I am printing PDF";
    }
    
    public function excel() {
	echo "I am printing Excel";
    }
    
    public function text() {
	echo "I am printing Text";
    }
    
    public function csv() {
	echo "I am printing CSV";
    }
    
    public function showUserName() {
	echo "i am from export";
    }
}

class Report {
    
    use Security, Export {
	Security::showUserName insteadof Export;
	Export::showUserName as ExportShowUserName;
    }
    
    
    function pdf() {
	echo "I am PDF from Report";
    }
    
}

$financialReport = new Report;

$financialReport->showUserName();
$financialReport->ExportShowUserName();