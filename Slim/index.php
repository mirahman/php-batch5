<?php

require 'vendor/autoload.php';


$app = new Slim\App();

$host = "localhost";
$user = "root";
$pass = "";
$dbname = "batchfive";

$dsn = "mysql:host=$host;dbname=$dbname";

$pdo =   new PDO($dsn, $user, $pass);

//function defination to convert array to xml
function array_to_xml($array, &$xml_user_info) {
    foreach($array as $key => $value) {
        if(is_array($value)) {
            if(!is_numeric($key)){
                $subnode = $xml_user_info->addChild("$key");
                array_to_xml($value, $subnode);
            }else{
                $subnode = $xml_user_info->addChild("book");
                array_to_xml($value, $subnode);
            }
        }else {
            $xml_user_info->addChild("$key",htmlspecialchars("$value"));
        }
    }
}

$app->post("/students/", function ($request, $response, $args) use ($pdo) {
    //$response->write("Drivers List");
    
    try {
    $post = $request->getParams();
    
     $sql = "INSERT INTO students "
        . " SET "
        ."student_name = ?,"
        . "student_email = ?";

$stmt = $pdo->prepare($sql);

$stmt->bindParam(1, $dName);
$stmt->bindParam(2, $license);


$dName = $post['name'];
$license = $post['email'];


$stmt->execute();

$error = $stmt->errorInfo();


if($error[1])
    throw new Exception("duplicate id");

$response->write("added successfully");

    return $response;
    
    }  catch (Exception $e){
	$newResponse = $response->withStatus(400);
    $newResponse->write("duplicate email");
    return $newResponse;
    }
});

$app->put("/updateDriver", function ($request, $response, $args) use ($pdo) {
    //$response->write("Drivers List");
    
    $post = $request->getParsedBody();
    
    //print_r($post);
    
     $sql = "UPDATE drivers "
        . " SET "
        ."driverName = ?,"
        . "licenseNumber = ?"
             . " WHERE id = ?";

$stmt = $pdo->prepare($sql);

$stmt->bindParam(1, $dName);
$stmt->bindParam(2, $license);
$stmt->bindParam(3, $id);

$dName = $post['name'];
$license = $post['license'];
$id = $post['id'];


$stmt->execute();
$response->write("updated successfully");

    return $response;
});

$app->get("/students/", function ($request, $response, $args) use ($pdo) {
    //$response->write("Drivers List");
    
    $result = $pdo->query("select * from students");

    $names = [];
    foreach ($result as $row) {
        $names[] = ["id" => $row['id'],"name" => $row['student_name']];
    }    

            $response->write(json_encode($names));
        
   
    
    //$response->write(json_encode($names));
    return $response;
});

$app->get('/', function ($request, $response, $args) {
    $response->write("Welcome to home page - mizan ");
    return $response;
});

$app->get('/show/{name}', function ($request, $response, $args) {
    $newResponse = $response->withStatus(500);
    $newResponse->write("Welcome  " . $args['name']);
    return $newResponse;
});

$app->run();
