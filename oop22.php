<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

abstract class Bio {
    abstract function showName();
}

interface Biox {
    public function showName();
}

class Cricketer extends Bio{
    
    public $name;
    public $position;
    public $age;
    
    public function __construct($name, $position, $age) {
        $this->name = $name;
        $this->position = $position;
        $this->age = $age;
    }
    
    public function showName() {
        echo $this->name;
    }
    
}

class NationalCricketer{
    
    public $division;
    
    public function __construct($name, $position, $age, $division) {
        //parent::__construct($name, $position, $age);
        $this->division = $division;
    }
    
    public function showName(Bio $cricketer) {
        echo "I am showing : ".$cricketer->name;
    }
    
}



$mosaddek = new Cricketer("Mosaddek Saikat", "All Rounder", 20);
$mosaddek1 = &$mosaddek; //new Cricketer("Mosaddek Saikat", "All Rounder", 20);


if($mosaddek === $mosaddek1) {
    echo "We are same";
} else {
    echo "We are not same";
}

$mosaddek->name = "Saikat";

echo $mosaddek->name."<br />";
echo $mosaddek1->name."<br />";

$mosaddek = NULL;

echo $mosaddek->name."<br />";
echo $mosaddek1->name."<br />";
