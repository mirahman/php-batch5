<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
error_reporting(E_ALL);

class Cricketer {
    
    public $data;
    
    public function __get($name) {
        if(isset($this->data[$name]))
            return $this->data[$name];
        
        echo "$name no property with this name";
    }
    
    public function __set($name, $value) {
        
        $this->data[$name] = $value;
         echo "$name can not be set";
    }
    
    public function __call($name, $arguments) {
        echo "$name does not exist";
    }
    
}

$mash = new Cricketer;

$mash->name = "Masrafee";

$mash->showName();
$mash->totalRun();

