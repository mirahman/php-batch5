<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Cricketer {
    
    public static function showName() {
	echo static::getName();
    }
    
    public static function getName() {
	return "Cricketer";
    }
}

class NationalCricketer extends Cricketer{
    
    public static function getName() {
	return "National Cricketer";
    }
   
}

Cricketer::showName();
NationalCricketer::showName();